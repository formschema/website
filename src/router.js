import Vue from 'vue'
import Router from 'vue-router'
import Demo from './views/Demo.vue'
import MdViewer from '@/components/MdViewer.vue'

Vue.use(Router)

export default new Router({
  base: process.env.NODE_ENV === 'production' ? '' : '/',
  routes: [
    {
      path: '/',
      name: 'home',
      component: MdViewer,
      props: {
        url: 'pages/Home.md'
      }
    },
    {
      path: '/github',
      name: 'github',
      beforeEnter () {
        window.location.href = 'https://github.com/formschema'
      }
    },
    {
      path: '/api',
      name: 'api',
      component: MdViewer,
      props: {
        url: 'pages/API.md'
      }
    },
    {
      path: '/getting-started',
      name: 'getting-started',
      component: MdViewer,
      props: {
        url: 'pages/GettingStarted.md'
      }
    },
    {
      path: '/features/ArrayInputs',
      name: 'ArrayInputs',
      component: MdViewer,
      props: {
        url: 'pages/features/ArrayInputs.md'
      }
    },
    {
      path: '/features/AsyncSchema',
      name: 'AsyncSchema',
      component: MdViewer,
      props: {
        url: 'pages/features/AsyncSchema.md'
      }
    },
    {
      path: '/features/CustomElements',
      name: 'CustomElements',
      component: MdViewer,
      props: {
        url: 'pages/features/CustomElements.md'
      }
    },
    {
      path: '/features/GroupedRadios',
      name: 'GroupedRadios',
      component: MdViewer,
      props: {
        url: 'pages/features/GroupedRadios.md'
      }
    },
    {
      path: '/features/MultipleCheckboxes',
      name: 'MultipleCheckboxes',
      component: MdViewer,
      props: {
        url: 'pages/features/MultipleCheckboxes.md'
      }
    },
    {
      path: '/features/RegexInputs',
      name: 'RegexInputs',
      component: MdViewer,
      props: {
        url: 'pages/features/RegexInputs.md'
      }
    },
    {
      path: '/demo/elements/:name',
      component: MdViewer,
      props: { url: '' },
      beforeEnter (to, from, next) {
        console.log('--->', `pages/elements/${to.params.name}.md`)
        console.log(arguments)
        next((vm) => {
          console.log('<<<---')
          vm.url = `pages/elements/${to.params.name}.md`
          console.log('<<<---', vm.url)
        })
      }
    },
    {
      path: '/demo/signin',
      component: Demo,
      props: {
        url: 'schema/signin.json'
      }
    },
    {
      path: '/demo/newsletter',
      component: Demo,
      props: {
        url: 'schema/newsletter.json'
      }
    },
    {
      path: '/license',
      name: 'license',
      component: MdViewer,
      props: {
        url: 'pages/License.md'
      }
    }
  ]
})
