export const get = (url) => new Promise((resolve, reject) => {
  const request = new XMLHttpRequest()

  request.open('GET', url, true)

  request.onload = () => {
    if (request.status >= 200 && request.status < 400) {
      return resolve(request.responseText)
    }

    reject(new Error('We reached our target server, but it returned an error'))
  }

  request.onerror = () => {
    reject(new Error('There was a connection error of some sort'))
  }

  request.send()
})

export const getJSON = (url) => get(url).then((data) => JSON.parse(data))
