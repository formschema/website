# FormSchema API

## props
- `schema` ***[Object, Promise]*** (*required*)
The JSON Schema object. Use the `v-if` directive to load asynchronous schema. 

- `v-model` ***Object*** (*optional*) `default: [object Object]` 
Use this directive to create two-way data bindings with the component. It automatically picks the correct way to update the element based on the input type. 

- `action` ***String*** (*optional*) 
The URI of a program that processes the form information. 

- `autocomplete` ***String*** (*optional*) 
This property indicates whether the value of the control can be automatically completed by the browser. Possible values are: `off` and `on`. 

- `enctype` ***String*** (*optional*) `default: 'application/x-www-form-urlencoded'` 
When the value of the method attribute is post, enctype is the MIME type of content that is used to submit the form to the server. Possible values are: - application/x-www-form-urlencoded: The default value if the attribute is not specified. - multipart/form-data: The value used for an <input> element with the type attribute set to "file". - text/plain (HTML5) 

- `method` ***String*** (*optional*) `default: 'post'` 
The HTTP method that the browser uses to submit the form. Possible values are: - post: Corresponds to the HTTP POST method ; form data are included in the body of the form and sent to the server. - get: Corresponds to the HTTP GET method; form data are appended to the action attribute URI with a '?' as separator, and the resulting URI is sent to the server. Use this method when the form has no side-effects and contains only ASCII characters. 

- `novalidate` ***Boolean*** (*optional*) 
This Boolean attribute indicates that the form is not to be validated when submitted. 

- `input-wrapping-class` ***String*** (*optional*) 
Define the inputs wrapping class. Leave `undefined` to disable input wrapping. 

## events
- `input` Fired synchronously when the value of an element is changed. 

- `change` Fired when a change to the element's value is committed by the user. 

- `invalid` Fired when a submittable element has been checked and doesn't satisfy its constraints. The validity of submittable elements is checked before submitting their owner form, or after the `checkValidity()` of the element or its owner form is called. 

- `submit` Fired when a form is submitted 

## methods
- `input(name)` 
Get a form input reference. 

- `form()` 
Get the form reference. 

- `reportValidity()` 
Returns true if the element's child controls satisfy their validation constraints. When false is returned, cancelable invalid events are fired for each invalid child and validation problems are reported to the user. 

- `checkValidity()` 
Checks whether the form has any constraints and whether it satisfies them. If the form fails its constraints, the browser fires a cancelable `invalid` event at the element, and then returns false. 

- `reset()` 
Reset the value of all elements of the parent form. 

- `submit(event)` 
Send the content of the form to the server. 

- `setErrorMessage(message)` 
Set a message error. 

- `clearErrorMessage()` 
clear the message error. 
