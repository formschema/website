# Grouped Radio elements

To group radio elements, use the [JSON Schema keyword `enum`](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.23) and `attrs.type === 'radio'`:

**schema.json**

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "groupedRadio": {
      "type": "string",
      "enum": [
        { "value": "daily", "label": "Daily New" },
        { "value": "promotion", "label": "Promotion" }
      ],
      "attrs": {
        "type": "radio"
      }
    }
  }
}
```

**component.vue**

```html
<script>
  import FormSchema from 'vue-json-schema'

  FormSchema.setComponent('select', 'el-radio', ({ item }) => {
    return { label: item.value }
  })

  FormSchema.setComponent('radiogroup', 'el-radio-group')

  export default { ... }
</script>
```
